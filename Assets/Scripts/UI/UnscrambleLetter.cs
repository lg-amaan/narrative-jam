﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class UnscrambleLetter : EventTrigger
{
    private bool dragging;

    public void Update()
    {
        if (dragging)
        {
            RectTransform parentRect = transform.parent.GetComponent<RectTransform>();
            RectTransform rect = GetComponent<RectTransform>();

            // keep the transform locked between
            Vector2 newPosition = new Vector2(Mathf.Clamp(Input.mousePosition.x, rect.rect.width / 2, Screen.width - (rect.rect.width / 2)), transform.position.y);
      
            transform.position = newPosition;
        }
    }

    public override void OnPointerDown(PointerEventData eventData)
    {
        dragging = true;
    }

    public override void OnPointerUp(PointerEventData eventData)
    {
        dragging = false;
    }
}
