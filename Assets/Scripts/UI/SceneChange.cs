﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class SceneChange : MonoBehaviour
{
    public Animator animator;
    public TextMeshProUGUI text;

    public void SwitchScenes(string chapterText)
    {
        text.text = chapterText;
        animator.SetTrigger("fadein");
    }

    public void ReachedFadePoint()
    {
        GameController.PlayAudio(TrackSource.BGM, "backing");

        Director.PlayStep(Director.instance.stepIndex);
    }
}
