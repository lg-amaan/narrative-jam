﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class NarrativePanel : MonoBehaviour
{
    public TextMeshProUGUI text;
    public TextMeshProUGUI characterNameText;

    public Image characterImage;
    public GameObject clickContinue;

    public bool skipped = false;

    public void SetText(string characterName, string narrative)
    {
        skipped = false;
        clickContinue.SetActive(false);

        characterNameText.text = characterName;
        text.text = narrative;
    }

    public void Skip()
    {
        if (!skipped)
        {
            skipped = true;
        }
        else
        {
            gameObject.SetActive(false);
            GameController.instance.character.Stop();
            Director.Continue();
        }
    }
}
