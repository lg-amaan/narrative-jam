﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class CloseupView : MonoBehaviour
{
    public Button closeButton;
    public Image imageSource;
    public TextMeshProUGUI textField;

    private bool showingImage = true;
    public void Init(Sprite sprite, string text)
    {
        GameController.PlayAudio(TrackSource.SFX, "pickup");

        imageSource.sprite = sprite;
        textField.text = text;

        showingImage = true;

        gameObject.SetActive(true);
    }

    public void Close()
    {
        gameObject.SetActive(false);

        Director.Continue();
    }

    public void ToggleText()
    {
        showingImage = !showingImage;

        imageSource.color = (showingImage) ? Color.white : new Color(142, 142, 142, 0.5f);
        textField.gameObject.SetActive(!showingImage);
    }
}
