﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.SceneManagement;
public enum DirectorState
{
    Chapter,
    Investigation
}

public class Director : MonoBehaviour
{
    public static Director instance;

    public DialogueScene dialogueScene1;
    public SceneChange sceneChange;
    public AudioMixer audioMixer;

    private List<SceneItem> scenes = new List<SceneItem>();

    private SceneItem current;

    public int sceneIndex = 0;
    public int stepIndex = 0;

    private List<CoreScene> investigationScenes = new List<CoreScene>();
    private int investigationindex = 0;

    private DirectorState directorState;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
    }

    public static void Init()
    {
        if (instance)
        {
            SceneItem[] sceneItems = instance.GetComponentsInChildren<SceneItem>(); // load all the scenes

            foreach (SceneItem sceneItem in sceneItems)
            {
                sceneItem.Init();
                instance.scenes.Add(sceneItem);
            }

            Debug.LogFormat("DIRECTOR: Loaded {0} scenes", instance.scenes.Count);
        }
    }

    public static void SetState(DirectorState state)
    {
        if (instance)
        {
            instance.directorState = state;

            if (state == DirectorState.Investigation)
            {
                GameController.PlayAudio(TrackSource.BGM, "investigate");
            }
            else
            {
                GameController.PlayAudio(TrackSource.BGM, "backing");
            }
        }
    }

    public static void LoadScene(int index, int step, bool showFade = false)
    {
        if (instance)
        {
            if (instance.scenes[index] != null)
            {

                instance.current = instance.scenes[index];

                if (showFade)
                {
                    int currentScene = PlayerPrefs.GetInt("chapter", 0);
                    if (currentScene < index)
                    {
                        // set the scene the user has got up to
                        PlayerPrefs.SetInt("chapter", index);
                    }

                    if (GameController.instance.chapterSelected)
                    {
                        // reload the menu
                        SceneManager.LoadScene("MainMenu");
                    }
                    else
                    {
                        instance.sceneChange.SwitchScenes(instance.current.chapter);

                        instance.StartCoroutine(GameController.StartFade(instance.audioMixer, "Volume", 2.0f, 0));

                        GameController.instance.sfx2.Stop();
                    }
                }
                else
                {
                    PlayStep(step);
                }
            }
            else
            {
                Debug.LogWarningFormat("DIRECTOR: No scene with index {0}", index);
            }
        }
    }

    public static void AddSteps(List<Dialogue> dialogues)
    {
        if (instance)
        {
            int index = 1;
            foreach(Dialogue dialogue in dialogues)
            {
                DialogueScene dialogueScene = Instantiate(instance.dialogueScene1, instance.current.scenes[instance.stepIndex].transform.parent.transform);
                dialogueScene.character = dialogue.character;
                dialogueScene.emotion = dialogue.emotion;
                dialogueScene.dialogue = dialogue.dialogue;
                dialogueScene.audioClip = dialogue.audioClip;

                instance.current.scenes.Insert(instance.stepIndex + index, dialogueScene);

                index++;
            }

            Continue();
        }
    }

    public static void AddSteps(GameObject steps)
    {
        int index = 1;
        CoreScene[] coreScenes = steps.GetComponentsInChildren<CoreScene>(); // load all the scenes
        foreach(CoreScene coreScene in coreScenes)
        {
            GameObject coreScene1 = Instantiate(coreScene.gameObject, instance.current.scenes[instance.stepIndex].transform.parent.transform);
            instance.current.scenes.Insert(instance.stepIndex + index, coreScene1.GetComponent<CoreScene>());
            index++;
        }

        Continue();
    }

    public static List<CoreScene> GetChapter(int chapter)
    {
        return instance.scenes[chapter].scenes;
    }

    public static void PlayStep(int index)
    {
        if (instance && instance.current)
        {
            int count = (instance.directorState == DirectorState.Chapter) ? instance.current.scenes.Count : instance.investigationScenes.Count;
            List<CoreScene> scenesTouse = (instance.directorState == DirectorState.Chapter) ? instance.current.scenes : instance.investigationScenes;

            if (index < count)
            {
                CoreScene scene = scenesTouse[index];

                if (scene)
                {
                    Debug.LogFormat("Playing {0}/{1}", index, count);

                    instance.StartCoroutine(scene.Play());
                }
            }
            else
            {
                if (instance.directorState == DirectorState.Chapter)
                {
                    // TODO: cleanup function?
                    instance.sceneIndex += 1;
                    instance.stepIndex = 0;

                    if (instance.sceneIndex >= instance.scenes.Count)
                    {
                        GameController.ShowEnd();
                    }
                    else
                    {
                        LoadScene(instance.sceneIndex, instance.stepIndex, true);
                    }
                }
                else
                {
                    instance.investigationindex = 0;

                    //CharacterManager.ToggleCharacters(false);
                    RoomManager.BlockInteraction(false);

                    RoomManager.CheckDone();
                }
            }
        }
    }

    public static void ChangeStepIndex(CoreScene cs)
    {
        int index = instance.scenes[instance.sceneIndex].scenes.IndexOf(cs);
        if (index > -1)
        {
            instance.stepIndex = index;
            Director.Continue();
        }
    }

    public static void PlayInvestigation(List<CoreScene> scenes)
    {
        if (instance)
        {
            //CharacterManager.ToggleCharacters(true);
            RoomManager.BlockInteraction(true);

            instance.investigationScenes = scenes;
            instance.investigationindex = 0;

            PlayStep(instance.investigationindex);
        }
    }

    public static void EndScene()
    {
        if (instance)
        {

        }
    }

    public static void Continue()
    {
        if (instance)
        {
            if (instance.directorState == DirectorState.Chapter)
            {
                instance.stepIndex += 1;
                PlayStep(instance.stepIndex);
            }
            else
            {
                instance.investigationindex += 1;
                PlayStep(instance.investigationindex);
            }
        }
    }

    public static void Cleanup()
    {
        
    }
}
