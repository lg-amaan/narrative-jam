﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public enum TrackSource
{
    BGM,
    SFX,
    SFX2,
    Character
}

public class GameController : MonoBehaviour
{
    public static GameController instance;

    public NarrativePanel narrativePanel;
    public Unscramble unscramble;
    public Hangman hangman;
    public EndingScreen endingScreen;

    public Image backgroundImage;

    public AudioSource BGM;
    public AudioSource sfx;
    public AudioSource sfx2;
    public AudioSource character;

    public List<SfxData> sfxDatas;
    public List<BackgroundData> backgroundDatas;

    public bool chapterSelected = false;

    private void Start()
    {
        if (instance == null)
        {
            instance = this;
            Init();
        }
    }

    void Init()
    {
        Director.Init();
    }

    public static void ShowEnd()
    {
        if (instance)
        {
            instance.endingScreen.Setup();
        }
    }

    public static void ReturnToMenu()
    {
        SceneManager.LoadScene("MainMenu");
    }

    public static void Play()
    {
        PlayAudio(TrackSource.BGM, "backing");
        Director.LoadScene(Director.instance.sceneIndex, Director.instance.stepIndex);
    }

    public static void ResetRelationshipDataForChapter(int chapter)
    {
        // set everything for a new session
        foreach (Character character in CharacterManager.instance.characters)
        {
            string name = CharacterManager.GetName(character.characterName);
            PlayerPrefs.DeleteKey(string.Format("{0}-{1}", name, Director.instance.sceneIndex));
        }
    }

    public static void SetupUnscramble(string word)
    {
        if (instance)
        {
            instance.unscramble.Setup(word);
        }
    }

    public static void StartHangman()
    {
        if (instance)
        {
            instance.hangman.Init();
        }
    }

    public static AudioClip GetAudio(string id)
    {
        if (instance)
        {
            SfxData sfxData = instance.sfxDatas.Find(x => x.id == id);

            if (sfxData == null)
            {
                Debug.LogWarningFormat("Missing audio {0}", id);
                return null;
            }

            return sfxData.audioClip;
        }

        return null;
    }

    public static Sprite GetBackgroundImage(string id)
    {
        if (instance)
        {
            BackgroundData backgroundData = instance.backgroundDatas.Find(x => x.id == id);

            if (backgroundData == null)
            {
                Debug.LogWarningFormat("Missing image {0}", id);
                return null;
            }

            return backgroundData.image;
        }

        return null;
    }

    public static void PlayAudio(TrackSource source, string id)
    {
        if (instance)
        {
            AudioClip audioClip = GetAudio(id);
            if (audioClip)
            {
                switch (source)
                {
                    case TrackSource.BGM:
                        if (instance.BGM.clip != audioClip)
                        {
                            if (!instance.BGM.isPlaying)
                            {
                                instance.BGM.clip = audioClip;
                                instance.BGM.Play();

                                instance.StartCoroutine(StartFade(Director.instance.audioMixer, "Volume", 1.0f, 1.0f));
                            }
                            else
                            {
                                instance.StartCoroutine(StartFade(Director.instance.audioMixer, "Volume", 1.0f, 0, audioClip));
                            }
                        }
                        break;
                    case TrackSource.SFX:
                        if (!instance.sfx.isPlaying)
                        {
                            instance.sfx.clip = audioClip;
                            instance.sfx.Play();
                        }
                        break;
                    case TrackSource.SFX2:
                        if (!instance.sfx2.isPlaying)
                        {
                            instance.sfx2.clip = audioClip;
                            instance.sfx2.Play();
                        }
                        break;
                }
            }
        }
    }

    public static void PlayCharacterAudio(AudioClip clip)
    {
        instance.character.Stop();
        instance.character.clip = clip;
        instance.character.Play();
    }

    public static IEnumerator StartFade(AudioMixer audioMixer, string exposedParam, float duration, float targetVolume, AudioClip newClip = null)
    {
        float currentTime = 0;
        float currentVol;
        audioMixer.GetFloat(exposedParam, out currentVol);
        currentVol = Mathf.Pow(10, currentVol / 20);
        float targetValue = Mathf.Clamp(targetVolume, 0.0001f, 1);

        while (currentTime < duration)
        {
            currentTime += Time.deltaTime;
            float newVol = Mathf.Lerp(currentVol, targetValue, currentTime / duration);
            audioMixer.SetFloat(exposedParam, Mathf.Log10(newVol) * 20);
            yield return null;
        }

        if (newClip != null)
        {
            instance.BGM.clip = newClip;
            instance.BGM.Play();
            instance.StartCoroutine(StartFade(audioMixer, exposedParam, duration, 1.0f));
        }

        yield break;
    }

    public static void SetBackgroundImage(string id)
    {
        if (instance)
        {
            Sprite sprite = GetBackgroundImage(id);
            if (sprite)
            {
                instance.backgroundImage.sprite = sprite;
            }
        }
    }

    public static NarrativePanel GetNarrativePanel()
    {
        if (instance)
        {
            return instance.narrativePanel;
        }

        return null;
    }
}
