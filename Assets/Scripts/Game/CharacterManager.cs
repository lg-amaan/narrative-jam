﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public enum CharacterName
{
    Jordan,
    Nora,
    Layton,
    Ambrose,
    Wyatt,
    None
}

public enum Emotion
{
    Happy,
    Sad,
    Angry,
    Neutral,
    Thinking,
    Scared,
    Dead
}


public enum Position
{
    FarLeft,
    Left,
    Middle,
    Right,
    FarRight,
    Offscreen
}

public class CharacterManager : MonoBehaviour
{
    public static CharacterManager instance;

    public List<Character> characters;
    public List<SlotType> slotTypes;

    public CanvasGroup characterCanvasGroup;

    private void Start()
    {
        if (instance == null)
        {
            instance = this;
        }
    }

    public static void UpdateRelationships(CharacterName characterName, int change)
    {
        Character character = instance.characters.Find(x => x.characterName == characterName);
        if (character)
        {
            character.UpdateRelationship(change);
        }
    }

    public static int GetRelationship(CharacterName characterName)
    {
        int current = 0;

        Character character = instance.characters.Find(x => x.characterName == characterName);
        if (character)
        {

            string name = GetName(characterName);
            for (int i = 0; i < Director.instance.sceneIndex; i++)
            {
                string id = string.Format("{0}-{1}", name, i);
                int relationship = PlayerPrefs.GetInt(id, 0);

                if (relationship > 0)
                {
                    current += relationship;
                }
            }
        }

        return current;
    }

    public static Sprite GetSprite(CharacterName characterName, Emotion emotion = Emotion.Neutral)
    {
        if (instance)
        {
            Character character = instance.characters.Find(x => x.characterName == characterName);
            if (character)
            {
                return character.GetEmotion(emotion);
            }

            return null;
        }

        return null;
    }

    public static string GetName(CharacterName characterName)
    {
        switch(characterName)
        {
            case CharacterName.Ambrose:
                return "Ambrose";
            case CharacterName.Jordan:
                return "Jordan";
            case CharacterName.Layton:
                return "Layton";
            case CharacterName.Nora:
                return "Nora";
            case CharacterName.Wyatt:
                return "Wyatt";
            default:
                return "?????";
        }
    }

    public static void PlaceInSlot(Position position, CharacterName characterName, Emotion emotion = Emotion.Neutral)
    {
        if (instance)
        {
            if (position == Position.Offscreen)
            {
                // find the slot they're in
                SlotType onscreenSlot = GetSlotWithCharacter(characterName);
                if (onscreenSlot)
                {
                    onscreenSlot.Clear();
                }
            }
            else
            {
                SlotType slot = GetSlotTransform(position);
                if (slot)
                {
                    Sprite characterSprite = GetSprite(characterName, emotion);

                    slot.Add(characterName, characterSprite);
                }
            }
        }
    }

    public static void ToggleCharacters(bool show)
    {
        if (instance)
        {
            instance.characterCanvasGroup.alpha = (show) ? 1 : 0;
        }
    }

    public static SlotType GetSlotTransform(Position position)
    {
        if (instance)
        {
            SlotType slotType = instance.slotTypes.Find(x => x.position == position);
            if (slotType)
            {
                return slotType;
            }

            return null;
        }

        return null;
    }

    public static SlotType GetSlotWithCharacter(CharacterName name)
    {
        if (instance)
        {
            SlotType slotType = instance.slotTypes.Find(x => x.current == name);
            if (slotType)
            {
                return slotType;
            }

            return null;
        }

        return null;
    }

    public static void UpdateTalking(CharacterName name)
    {
        if (instance)
        {
            foreach (SlotType slot in instance.slotTypes)
            {
                if (slot.current != CharacterName.None && slot.current != name)
                {
                    slot.Grayscale(1);
                }
            }
        }
    }
}
