﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChoiceManager : MonoBehaviour
{
    public static ChoiceManager instance;
    public ChoicePanel choicePanel;

    private List<Choice> currentChoices;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
    }

    public static void SetupChoiceButtons(List<Choice> choices)
    {
        if (instance)
        {
            instance.choicePanel.Setup(choices);
            instance.currentChoices = choices;
        }
    }

    public static void HandleChoiceInput(string text)
    {
        if (instance)
        {
            Choice choice = instance.currentChoices.Find(x => x.choiceText == text);
            if (choice != null)
            {
                if (choice.amount > 0)
                {
                    GameController.PlayAudio(TrackSource.SFX, "good-choice");
                }
                else if (choice.amount == -1)
                {
                    GameController.PlayAudio(TrackSource.SFX, "bad-choice");
                }

                if (choice.amount > 0 || choice.amount == -1)
                    CharacterManager.UpdateRelationships(choice.affected, choice.amount);

                if (choice.dialogues.Count == 0)
                    Director.Continue();

                if (choice.dialogues.Count > 0)
                    GenerateText(choice.dialogues);

                if (choice.steps != null)
                    Director.AddSteps(choice.steps);

                // store the ending to load
                if (!string.IsNullOrEmpty(choice.endingId))
                    PlayerPrefs.SetString("ending", choice.endingId);
            }
            else
            {
                Debug.LogWarningFormat("No choice found for {0}", text);
            }
        }
    }

    public static void GenerateText(List<Dialogue> dialogues)
    {
        // insert into the current index of Director
        Director.AddSteps(dialogues);
    }
}
