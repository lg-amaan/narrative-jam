﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SlotType : MonoBehaviour
{
    public Position position;
    public CharacterName current = CharacterName.None;

    private Image image;
    private Animator animator;

    private void Start()
    {
        image = GetComponent<Image>();
        animator = GetComponent<Animator>();
    }

    public void Add(CharacterName name, Sprite sprite)
    {
        if (current == CharacterName.None)
            animator.SetTrigger("show");

        current = name;

        image.sprite = sprite;

        Grayscale(1);
    }

    public void UpdateImage(Sprite sprite)
    {
        image.sprite = sprite;

        Grayscale(0);
    }

    public void Clear()
    {
        animator.SetTrigger("hide");

        current = CharacterName.None;
    }

    public void Grayscale(int amount)
    {
        image.material.SetFloat("_GrayscaleAmount", amount);
    }

}
