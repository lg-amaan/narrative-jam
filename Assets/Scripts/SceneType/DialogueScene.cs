﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class DialogueScene : CoreScene
{
    public CharacterName character;
    public Emotion emotion;

    public string dialogue;
    public AudioClip audioClip;

    public override IEnumerator Play()
    {
        string characterName = CharacterManager.GetName(character);
        Sprite sprite = CharacterManager.GetSprite(character, emotion);

        if (characterName != null)
        {
            // are they on the screen? if so update their image
            SlotType slotType = CharacterManager.GetSlotWithCharacter(character);
            if (slotType)
            {
                slotType.UpdateImage(sprite);
            }

            CharacterManager.UpdateTalking(character);

            NarrativePanel narrativePanel = GameController.GetNarrativePanel();

            narrativePanel.gameObject.SetActive(true);
            narrativePanel.SetText(characterName, dialogue);

            yield return null;

            int totalVisibleCharacters = narrativePanel.text.textInfo.characterCount;
            int counter = 0;

            GameController.PlayCharacterAudio(audioClip);

            while (counter <= totalVisibleCharacters && !narrativePanel.skipped)
            {
                int visibleCount = counter % (totalVisibleCharacters + 1);

                narrativePanel.text.maxVisibleCharacters = visibleCount;

                if (visibleCount >= totalVisibleCharacters)
                    yield return new WaitForSeconds(1.0f);

                counter += 1;

                yield return new WaitForSeconds(0.05f);

                if (visibleCount < totalVisibleCharacters)
                {
                    int current = visibleCount;
                    TMP_CharacterInfo characterInfo = narrativePanel.text.textInfo.characterInfo[(current > 0) ? current - 1 : current];
                    if (char.IsPunctuation(characterInfo.character) && (characterInfo.character != '’' && characterInfo.character.ToString() != "'" && characterInfo.character != '(' && characterInfo.character != ')'))
                    {
                        yield return new WaitForSeconds(0.3f);
                    }
                }
            }

            if (narrativePanel.skipped)
            {
                narrativePanel.text.maxVisibleCharacters = totalVisibleCharacters;
            }

            narrativePanel.skipped = true;
            narrativePanel.clickContinue.SetActive(true);
        }
    }
}
