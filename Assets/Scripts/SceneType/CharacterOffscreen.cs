﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterOffscreen : CoreScene
{
    public bool offscreen;
    public override IEnumerator Play()
    {
        CharacterManager.ToggleCharacters(!offscreen);

        Director.Continue();

        yield return null;
    }
}
