﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicScene : CoreScene
{
    public string id;
    public TrackSource track;

    public override IEnumerator Play()
    {
        GameController.PlayAudio(track, id);

        Director.Continue();

        yield return null;
    }
}
