﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundChangeScene : CoreScene
{
    public string id;

    public override IEnumerator Play()
    {
        GameController.SetBackgroundImage(id);

        Director.Continue();

        yield return null;
    }
}
